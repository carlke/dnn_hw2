import theano
import theano.tensor as T
import numpy as np
floatX = theano.config.floatX

from theano import shared
from sklearn.externals import joblib

def relu(x):
    return T.switch(x < 0, 0, x)

def softmax(M):
    Mexp = T.exp(M)
    return Mexp / Mexp.sum(axis=1).reshape((Mexp.shape[0], 1))

class RNN(object):
    def __init__(self, n_in, n_h, n_out, input, activation=T.tanh):
        W_i_value = np.asarray(np.random.uniform(
            low=-np.sqrt(.01/(n_in + n_h)),
            high=np.sqrt(.01/(n_in + n_h)),
            size=(n_in, n_h)
        ), dtype=floatX)

        W_o_value = np.asarray(np.random.uniform(
            low=-np.sqrt(.01/(n_h + n_out)),
            high=np.sqrt(.01/(n_h + n_out)),
            size=(n_h, n_out)
        ), dtype=floatX)
        b_o_value = np.zeros((n_out,), dtype=floatX)

        W_h_value = np.asarray(np.identity(n_h), dtype=floatX)
        b_h_value = np.zeros((n_h,), dtype=floatX)

        W_i = shared(W_i_value, name='W_i')
        W_o = shared(W_o_value, name='W_o')
        b_o = shared(b_o_value, name='b_o')
        W_h = shared(W_h_value, name='W_h')
        b_h = shared(b_h_value, name='b_h')

        self.n_in = n_in
        self.n_h = n_h
        self.n_out = n_out
        self.input = input
        self.activation = activation

        self.W_i = W_i
        self.W_o = W_o
        self.b_o = b_o
        self.W_h = W_h
        self.b_h = b_h
        self.parameters = [self.W_i, self.W_o, self.b_o, self.W_h, self.b_h]

        def step(x_t, a_tm1, y_tm1):
            a_t = self.activation( T.dot(x_t, self.W_i) + \
                                   T.dot(a_tm1, self.W_h) + self.b_h)
            y_t = T.dot(a_t, self.W_o) + self.b_o
            return a_t, y_t

        a_0 = shared(np.asarray(np.zeros(n_h), dtype=floatX))
        y_0 = shared(np.asarray(np.zeros(n_out), dtype=floatX))

        [self.a_seq, self.y_seq],_ = theano.scan(
                                            fn=step,
                                            sequences=self.input,
                                            outputs_info=[a_0, y_0],
                                            #truncate_gradient=-1
                                    )

        #self.y_seq_last = self.y_seq[-1]
        self.p_y_given_x = softmax(self.y_seq)
        self.pred = T.cast(T.argmax(self.p_y_given_x, axis=1), dtype='int32')

    def errors(self, y):
        return T.mean(T.neq(self.pred, y))

    def predict(self, x):
        fpred = theano.function(
            inputs=[self.input],
            outputs=self.pred
        )
        return fpred(x)

    def save(self, filename):
        joblib.dump(self, filename)

    def load(self, filename):
        rnn = joblib.load(filename)
        for attr in dir(rnn):
            if "__" not in attr:
                self.__setattr__(attr, rnn.__getattribute__(attr))

def load(filename):
    rnn = RNN(1,1,1,T.matrix())
    rnn.load(filename)
    return rnn
