import theano
import utils
import model
import sys
floatX = theano.config.floatX

import numpy as np
import theano.tensor as T

from theano import shared
#from sklearn.cross_validation import KFold
from time import time

print('Loading data...')
t = time()
x_seq, y_hat, n2lab, ids = utils.loadData(sys.argv[1])
SI = utils.getSentIndice(ids)
print('Loaded. Time elaspsed: %.2f secs' % (time()-t))

#folds = KFold(y_hat, n_folds=5)

for _ in range(1):
    boundary = (y_hat.shape[0] / 5) * 4
    print('train/test size: (%d, %d)' % (boundary, y_hat.shape[0]-boundary))
    x_train = shared(np.asarray(x_seq[:boundary+1], dtype=floatX))
    x_test = shared(np.asarray(x_seq[boundary+1:], dtype=floatX))
    y_train = T.cast(shared(np.asarray(y_hat[:boundary+1], dtype=floatX)), 'int32')
    y_test = T.cast(shared(np.asarray(y_hat[boundary+1:], dtype=floatX)), 'int32')

    print('  Building model...')
    t = time()
    x = T.matrix('x')
    y = T.ivector('y')
    rnn = model.RNN(x_seq.shape[1], 500, 48, x)
    pyx = rnn.p_y_given_x
    NLL = -T.mean(T.log(pyx)[T.arange(y.shape[0]), y])
    cost = NLL

    gradients = T.grad(cost, rnn.parameters)
    learning_rate = np.float32(0.001)
    updates = [
            (param, param - learning_rate * grad.clip(-0.1, 0.1)) for
             param, grad in zip(rnn.parameters, gradients)
    ]

    rnn_test_error = theano.function(
                        inputs=[],
                        outputs=rnn.errors(y),
                        givens={x: x_test, y: y_test}
                    )
    rnn_train_error = theano.function(
                        inputs=[],
                        outputs=rnn.errors(y),
                        givens={x: x_train, y: y_train}
                    )

    sindex = T.iscalar('sindex')
    eindex = T.iscalar('eindex')
    rnn_train = theano.function(
                        inputs=[sindex, eindex],
                        outputs=cost,
                        updates=updates,
                        givens={
                            x: x_train[ sindex : eindex ],
                            y: y_train[ sindex : eindex ]
                        }
                    )
    #ftest = theano.function([],T.log(pyx).sum(axis=0)[:100],givens={x:x_train})
    print('  Model built. Time elapsed: %.2f secs' % (time()-t))
    #print(ftest())
    #print(ftest().dtype)
    #exit(0)

    print('  Training...')
    best_Eout = 1.
    for epoch in range(10000):
        t = time()
        costs = []
        for i in range(SI.shape[0]-1):
            if SI[i+1] < boundary:
                c = rnn_train(SI[i],SI[i+1])
                costs.append(c)
                #print('  %d: cost = %f' % (i,c))
        avg_cost = np.array(costs).mean()
        Ein = rnn_train_error()
        Eout = rnn_test_error()
        print('  Epoch %d:\n  cost: %f, Ein: %f, Eout: %f, time: %.1f secs' 
                    % (epoch+1, avg_cost, Ein, Eout, time()-t))
	if Eout < best_Eout:
            best_Eout = Eout
            rnn.save('rnn.model')
