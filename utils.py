"""
Contains some utility functions for hw1.

"""
import numpy as np
import csv

from sklearn.preprocessing import scale

def loadData(mergedFile, scaling=True):
    """
    Load merged data(Feature vectors, LableIDs, LableID-to-Lable mapping and FrameIDs).
    Will scale data feature-wise by default. Loading merged fbank takes about 60 secs.

    Usage: features, Lables, Lable2n, IDs = loadData(mergedFile)

    """
    with open(mergedFile) as f:
        lines = f.readlines()
    labels = []
    label2n = {}
    ids = []
    vecs = []
    for line in lines:
        split = line.split()
        if split[0] not in label2n:
            label2n[int(split[0])] = split[1]
        labels.append(int(split[0]))
        ids.append(split[2])
        vec = []
        for i in range(3,len(split)):
            vec.append(float(split[i]))
        vec = np.array(vec, dtype='float64')
        vecs.append(vec)
    labels = np.array(labels, dtype='int32')
    vecs = np.array(vecs)
    ids = np.array(ids)
    label2n = dict2array(label2n)

    if scaling:
        # feature-wise scaling
        for i in range(vecs.shape[1]):
            scale(vecs[:,i], copy=False)

    vecs = np.asarray(vecs, dtype='float32')

    return [vecs, labels, label2n, ids]

def getSentIndice(ids):
    l = []
    sent_ids = []
    for i in range(ids.shape[0]):
        split = ids[i].split('_')
        if split[2] == '1':
            l.append(i)
            sent_ids.append(split[0]+'_'+split[1])
    l.append(ids.shape[0])
    l = np.asarray(l, dtype='int32')
    sent_ids = np.array(sent_ids)
    return l, sent_ids

def getSentenceID(frameID):
    return frameID.split('_')[0] + '_' + frameID.split('_')[1]

def genFrameWindow(vecs, labels, ids, window_size=5):
    window_vecs = []
    window_labels = []
    window_ids = []
    for i in range(vecs.shape[0]):
        # check global boundary
        if (i-window_size) >= 0 and (i+window_size) < vecs.shape[0]:
            # check sentence boundary
            sentenceID1 = getSentenceID(ids[i-window_size])
            sentenceID2 = getSentenceID(ids[i])
            sentenceID3 = getSentenceID(ids[i+window_size])
            if sentenceID1 == sentenceID2 and sentenceID2 == sentenceID3:
                temp = np.array([])
                for j in range(-window_size, window_size+1):
                    temp = np.hstack([temp, vecs[i+j]])
                window_vecs.append(temp)
                window_labels.append(labels[i])
                window_ids.append(sentenceID2)
    window_vecs = np.asarray(window_vecs, dtype='float32')
    window_labels = np.asarray(window_labels, dtype='int32')
    window_ids = np.array(window_ids)
    return [np.asarray(window_vecs), window_labels, window_ids]

def genTestFrameWindow(vecs, ids, window_size=5):
    window_vecs = []
    window_ids = []
    for i in range(vecs.shape[0]):
        # check global boundary
        if (i-window_size) >= 0 and (i+window_size) < vecs.shape[0]:
            # check sentence boundary
            sentenceID1 = getSentenceID(ids[i-window_size])
            sentenceID2 = getSentenceID(ids[i])
            sentenceID3 = getSentenceID(ids[i+window_size])
            if sentenceID1 == sentenceID2 and sentenceID2 == sentenceID3:
                temp = np.array([])
                for j in range(-window_size, window_size+1):
                    temp = np.hstack([temp, vecs[i+j]])
                window_vecs.append(temp)
                window_ids.append(ids[i])
            else:
                window_vecs.append(np.cast['float32'](
                    np.hstack(vecs[0:(2*window_size+1)]))
                )
                window_ids.append(ids[i])
        else:
            window_vecs.append(np.cast['float32'](
                np.hstack(vecs[0:(2*window_size+1)]))
            )
            window_ids.append(ids[i])


    window_vecs = np.asarray(window_vecs, dtype='float32')
    window_ids = np.array(window_ids)
    return [np.asarray(window_vecs), window_ids]

def dict2array(d):
    l = []
    for i in range(len(d)):
        l.append(d[i])
    l = np.array(l)
    return l

def loadFeatureData(featureFile, scaling=True):
    """
    Load feature data (train.ark or test.ark)
    Usage: features, IDs = loadFeatureData(featureFile)

    """
    with open(featureFile) as f:
        lines = f.readlines()

    ids = []
    vecs = []
    for line in lines:
        split = line.split()
        ids.append(split[0])
        vec = []
        for i in range(1,len(split)):
            vec.append(float(split[i]))
        vec = np.array(vec, dtype='float64')
        vecs.append(vec)
    vecs = np.array(vecs)
    ids = np.array(ids)

    if scaling:
        for i in range(vecs.shape[1]):
            scale(vecs[:,i], copy=False)

    vecs = np.asarray(vecs, dtype='float32')

    return [vecs, ids]

def map48to39(mapping):
    with open('48_39.map') as f:
        lines = f.readlines()

    d = {}
    for line in lines:
        split = line.split()
        d[split[0]] = split[1]

    l = []
    for i in range(len(mapping)):
        mapped_label = d[mapping[i]]
        l.append(mapped_label)
    l = np.array(l)

    return l

def map39toChr(n2lab):
    with open('mapping') as f:
        lines = f.readlines()
    d = {}
    for line in lines:
        split = line.split()
        d[split[0]] = split[2]
    l = []
    n2lab = map48to39(n2lab)
    for i in range(len(n2lab)):
        mapped_label = d[n2lab[i]]
        l.append(mapped_label)
    l = np.array(l)
    return l

def trim(preds, SI, map48):
    preds_trim = []
    mapping = map48to39(map48)
    l_mapping = list(mapping)
    l_mapping.append('NaP')
    mapping = np.array(l_mapping)
    for i in range(SI.shape[0]-1):
        preds_sent = preds[SI[i]:SI[i+1]]
        preds_sent = np.trim_zeros(preds_sent)
        preds_sent_trim = []
        last_label = 48
        for j in range(len(preds_sent)):
            if mapping[preds_sent[j]] != mapping[last_label]:
                preds_sent_trim.append(preds_sent[j])
                last_label = preds_sent[j]
        preds_trim.append(preds_sent_trim)
    preds_trim = np.array(preds_trim)
    return preds_trim

def outputCSV(ids, preds, map48, outFile='output.csv', debug=False):
    """
    Generate CSV file for submission. Default output file name is 'output.csv'.
    Usage: outputCSV(frameIDs, predictions(number), number-to-label mapping)

    """
    SI, sent_ids = getSentIndice(ids)
    preds = trim(preds, SI, map48)
    if debug:
        mapChr = map48to39(map48)
    else:
	map39 = map48to39(map48)
        mapChr = map48toChr(map39)
    with open(outFile,'w') as f:
        writer = csv.DictWriter(f, fieldnames=['id','phone_sequence'])
        writer.writeheader()
        for i in range(len(preds)):
            labs = mapChr[preds[i]]
            temp = ''
            for j in range(len(labs)):
                temp += labs[j]
                if debug:
                    temp += ' '
            writer.writerow({'id':sent_ids[i], 'phone_sequence':temp})
